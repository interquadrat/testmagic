#!/usr/bin/env bash

# This script sets up the development environment.
# 2023-01-18 András Aszódi

# The default name of the virtual environment
VENV_DEFAULT="testmagic.venv"

# -- Functions --

print_help() {
    local scr="devsetup.sh"
    cat <<HELP
Usage: $scr create [\$VENV] | install
$scr create [\$VENV]: (Re)creates the virtual environment in \$VENV (default "$VENV_DEFAULT").
    Moves the old \$VENV directory to \${VENV}.BACKUP if it exists.
    Run this first.
$scr install [\$VENV]: Installs the package in "development mode" into
    the virtualenv directory \$VENV (default "$VENV_DEFAULT").
    Run this after '$scr create'
HELP
}

# Checks whether the arguments (program names) are in the user's $PATH,
# exits if not found. 
exitif_notinpath() {
    local missing=0
    
    # iterate over all arguments
    until [[ -z "$1" ]]; do
        if [[ ! -x "$(command -v $1)" ]]; then
            # no luck
            echo "No $1 in your PATH"
            let missing+=1
        fi
        shift
    done
    if [[ $missing -gt 0 ]]; then
        echo "Please install and then try again."
        exit 991
    fi
}

create_venv() {
    if [[ -d ${venv} ]]; then
        bkupdir=${venv}.BACKUP
        rm -rf ${bkupdir}
        mv ${venv} ${bkupdir}
        echo "Saved old virtual env directory as ${venv}.BACKUP"
    fi
    
    # upgrade pip and setuptools to latest available
    # NOTE: this needs Python >=3.9 because of the `--upgrade-deps` option
    python3 -m venv  --upgrade-deps --system-site-packages ${venv}
    echo "Now run './devsetup.sh install'"
}

install_package() {
    # install the package, including the optional R dependency
    if [[ "$VIRTUAL_ENV" == "" ]]; then
        source ${venv}/bin/activate
    fi
    python3 -m pip install -e .[R]
    deactivate
    echo "Now activate the virtual environment by running 'source ${venv}/bin/activate'."
}

# == MAIN ==

if [[ $# < 1 ]]; then
    print_help
    exit 1
fi

exitif_notinpath python3 pip3 R

venv=${2:-$VENV_DEFAULT}
case $1 in
    "create")  create_venv ;;
    "install") install_package ;;
    *) print_help ;;
esac

