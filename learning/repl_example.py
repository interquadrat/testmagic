#!/usr/bin/env python3

"""
REPL example script.
Try out the InteractiveConsole class from the built-in `code` module interactively :-)
:author: András Aszódi
:date: 2020-11-03
"""

from code import InteractiveConsole

# == MAIN ==

InteractiveConsole().interact(banner="Welcome to the REPL example", exitmsg="Good bye")
