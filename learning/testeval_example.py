#!/usr/bin/env python3

"""
Demonstrates how to evaluate an expression and test its result.
:author: András Aszódi
:date: 2020-11-03
"""

import unittest
from code import InteractiveInterpreter
from textwrap import dedent

def run_test(tcls):
    suite = unittest.TestLoader().loadTestsFromTestCase(tcls)
    runner = unittest.TextTestRunner(verbosity=0)
    runner.run(suite)

# -- Classes --

class HarnessInterpreter(InteractiveInterpreter):
    """
    Specialised Python interpreter for wrapping an expression
    in a test harness.
    """

    _HARNESS = """\
    @run_test
    class TestEval(unittest.TestCase):
        def test_expr(self):
            result = {} # exprstr goes here
            self.assertEqual({}, result)
    """

    def __init__(self):
        super().__init__(globals())

    def execute(self, exprstr, val=42):
        """
        Executes the expression within the test harness.
        :param exprstr: A Python expression which is supposed to evaluate to `val`
        :param val: The expected value of `exprstr` when evaluated
        :return: True if more input needed, False if evaluation worked (or not).
        """
        testexpr = dedent(self._HARNESS).format(exprstr, val)
        return self.runsource(testexpr)

# == MAIN ==

# Poor man's REPL
VAL = 42
intp = HarnessInterpreter()
print("Welcome to the Poor Man's test harness REPL")
while True:
    try:
        exprstr = input("Enter an expression that evaluates to {} (Ctrl-C to quit): ".format(VAL))
        retval = intp.execute(exprstr, VAL)
        if retval:
            print("More input needed... I don't know how to prompt for it :-)")
            continue
    except KeyboardInterrupt:
        break

print("\nGood bye")