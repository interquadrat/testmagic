#!/usr/bin/env python3

"""
Tests whether the code.InteractiveInterpreter class has a state.
:author: András Aszódi
:date: 2020-11-04
"""

from code import InteractiveInterpreter

# -- Class --

class Interp(InteractiveInterpreter):
    """
    Simple interpreter class.
    """
    def __init__(self):
        super().__init__(globals())

    def run(self, command):
        retval = self.runsource(command)
        if retval:
            print("Warning: incomplete command")
        else:
            print("Executed command: '{}'".format(command))

# == MAIN ==

intp = Interp()
intp.run("a=42")
intp.run("print(2*a)")  # does it remember a? Yes
intp.run("s = (3,6")    # incomplete