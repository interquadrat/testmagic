#!/usr/bin/env python3

"""
Example of how to use the `run_test` decorator
to run a unit test.
If you run this script, the output will be:

testToPow (__main__.Samples) ... ok

----------------------------------------------------------------------
Ran 1 test in 0.000s

OK

See https://stackoverflow.com/a/64657782
:author: András Aszódi
:date: 2020-11-03
"""

import unittest

# -- Functions --

def run_test(tcls):
    """
    Runs unit tests from a test class if the class is decorated with @run_test
    :param tcls: A class, derived from unittest.TestCase
    """
    suite = unittest.TestLoader().loadTestsFromTestCase(tcls)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)

# -- Classes --

@run_test
class Samples(unittest.TestCase):
    def testToPow(self):
        pow3 = 3**3
        self.assertEqual(pow3, 27)