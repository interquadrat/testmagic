"""
This module provides a class for running R exercises in an IPython notebook cell.
:author: András Aszódi
:date: 2023-05-20
"""

# Implementation note: the class is based on the IPython manual
# See https://ipython.readthedocs.io/en/stable/config/custommagics.html

from sys import stderr
import json

from IPython.core.magic import Magics, magics_class, cell_magic

from ..checker import DEFAULT_TOL
from ..exercise import ExerMagic
from .harness import RHarness

# -- Classes --

@magics_class
class RExerMagic(ExerMagic):
    """
    Defines magics to run a piece of R code wrapped in a unit test harness.
    """

    def __init__(self, tests=None, keyfile=None, rel_tol=DEFAULT_TOL, logfile=None):
        """
        Initialiser. Registers the created instance with IPython.
        :param tests: Either a dictionary with test name - expected value pairs,
            or the name of a JSON file from which such a dictionary can be `load`-ed.
            If None or omitted then no tests are registered (this is the default).
        :param keyfile: If not the default (`None`), then this string is supposed
            to be an encryption key file with which the contents of
            the `tests` obfuscated JSON file can be decrypted.
        :param rel_tol: The relative tolerance to be used in comparing floats.
        :param logfile: If specified, test success/failure will be logged to this file.
            If not specified (None), no logging is performed.
        """
        super().__init__(tests, keyfile, rel_tol, logfile)

    @cell_magic
    def rexer(self, testname, cell):
        """
        IPython 'cell magic' to wrap a piece of R code (sequence of expressions)
        in a unit test harness and have it run.
        Usage within an IPython cell:
        +-------------------------+
        | %%rexer testname        |
        | R code line             |
        | ... more code lines ... |
        +-------------------------+

        :param testname: Identifies the test to be used.
        :param cell: Contents of the cell
        """
        # Create an RHarness instance and test what's in the cell
        harness = RHarness(self._reltol)
        self._run_exercise(harness, testname, cell)
        self._last = harness.last
        return None     # suppress printing the result in a Notebook


