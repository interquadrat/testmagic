"""
Module providing the R language exercise harness class 'RHarness'.
:author: András Aszódi
:date: 2022-08-25
"""
import re
from textwrap import dedent

import rpy2.robjects
import rpy2.rlike.container

### NOTE: RPy2 converts R objects to Python objects differently
### depending on whether NumPy is installed or not.
### This is independent from whether NumPy is actually `import`-ed
### (for instance TestMagic does not need NumPy).
### See the corresponding comments in front of the `_pythonize` static method.
### We set a module-wide flag to instruct `RHarness` to use the proper conversions.

# NumPy detection logic
HAS_NUMPY = False
try:
    import numpy as np
    HAS_NUMPY = True
except ImportError:
    pass

from ..ipython import IPYTHON
from ..harness import Harness
from ..checker import DEFAULT_TOL

class RHarness(Harness):
    """
    This class runs R code from an IPython cell
    and check its result.
    """
    
    def __init__(self, rel_tol=DEFAULT_TOL):
        """
        Creates an RHarness instance.
        :param rel_tol: The relative tolerance to be used in comparing floats.
        """
        super().__init__(rel_tol=rel_tol)

    def get_trial_context(self):
        """
        Sets up a temporary "trial" execution context and returns it.
        When this is passed to `_exec_script()`, then the code executed
        is not influencing the global interpreter's state.
        :return: A string with the name of a temporary R environment (hard-coded)
            or None if it didn't work
        """
        CLONEFUNC = dedent("""\
        clone_env <- function(envir, deep = T) {
            if(deep) {
                clone <- list2env(
                    rapply(as.list(envir, all.names = T), 
                        clone_env, 
                        classes = "environment", how = "replace"
                    ), 
                    parent = parent.env(envir)
                )
            } else {
                clone <- list2env(as.list(envir, all.names = T), parent = parent.env(envir))
            }
            attributes(clone) <- attributes(envir)
            return(clone)
        }
        """)
        tempenv_name = "temp.env"
        CLONEINV = dedent(f"""\
        {tempenv_name} <- NULL  # if it's already in global namespace
        {tempenv_name} <- clone_env(globalenv())
        """)
        try:
            self._exec_script(CLONEFUNC + CLONEINV)    # run in current working environment
            return tempenv_name
        except Exception as err:
            return None
        
    # -- "hidden" --

    def _exec_script(self, script, ctx=None, printres=False):
        """
        Executes an R script and returns the value of the last expression,
        or None if the last command was not an expression.
        :param script: R code as a possibly multi-line string.
        :param ctx: The execution context. If `None` (default) or `False`, then
            the currently active R environment is used. If `True`,
            then a temporary R environment is used to evaluate `script`.
        :param printres: The last result of a tested expression is printed to `stdout`
            (do not print by default)
        :return: The value of the last expression seen, or None
        """
        try:
            # The %%R cell magic always returns None, we need %R line magic
            # and convert the multi-line `script` to a one-liner on the R side
            charvec = RHarness._rlines_to_charvec(script)
            if charvec is None:
                return None
            # If a "trial context" was required
            # then ask `eval` to run in a clone of the current environment
            if ctx:
                # run the code in the temporary environment
                oneliner = f"eval(parse(text={charvec}), envir={ctx})"                
            else:
                # just execute in current environment
                oneliner = f"eval(parse(text={charvec}))"
            
            # NOTE: requires RPy2 version >= 3.5.9
            with rpy2.robjects.default_converter.context():
                result = IPYTHON.instance.run_line_magic("R", oneliner)
                if printres and result is not None:
                    # hopefully the RPy2 objects have sane __str__ or __repr__ methods
                    print(result)
            # convert to something Python-like by hand
            ### to check if something new and bizarre emerges:
            ### print(f"\n*** Type of result: \"{type(result)}\", its value:\n{result}")
            return RHarness._pythonize(result)
        except rpy2.rinterface_lib._rinterface_capi.RParsingError as perr:
            raise ValueError(f"R could not parse \"{oneliner}\"\n{str(perr)}")
        except (KeyboardInterrupt, SystemExit):
            return None    # ignore them silently

    @staticmethod
    def _rlines_to_charvec(script):
        """
        Convert the multi-line string `script` to the string representation
        of an R character vector whose elements are the original lines.
        This can be passed to an `eval(parse(text=...))` construct
        which will be run by the Rpy2 `%R` line magic in `_exec_script`.
        :param script: A multi-line string containing R code
        :return: R code for a character vector containing the lines to be parsed
            or None if no R code lines were found.
        """
        # Break `script` up into its constituent lines,
        # surround them with "-s, skip empty and whitespace-only lines
        def _quote(c):
            cq = c.replace(r'"', r'\"')
            return f'"{cq}"'
        script = script.replace(";", "\n")  # R's `eval` doesn't like ";" in the token vector elements
        lines = [_quote(c) for c in script.splitlines() if len(c) > 0 and not re.match(r"^[ \t]+$", c)]
        if len(lines) == 0:
            return None
        
        # NOTE: Rpy2's %R one-liner magic has a bug (at least in V3.5.10):
        # if it sees an indexing construct like `x[, -3]
        # it raises an exception like "UsageError: unrecognized arguments: -3])"
        # the row index may be empty or non-empty, may contain spaces, it doesn't matter
        # it is the [ ...,_-i] ("_" indicates space) that triggers the bug
        fixlines = [ re.sub(r"\[(.*), +(-.*)\]", r"[\1,\2]", line) for line in lines ]
        if len(fixlines) == 1:
            return fixlines[0]
        charvec = "c(" + ",".join(fixlines) + ")"
        return charvec

    # -- Conversions --
    
    # If NumPy is installed (i.e. it can be `import`-ed) 
    # then RPy2 converts **some** of its R objects to NumPy objects as follows:
    # R vectors --> np.array with sensible dtype, except logical vectors
    #   where dtype=np.bool_ but the values are stored as integers and not bool-s
    # R matrices and n>=3 dimensional arrays --> multidimensional np.array
    # R lists --> rpy2.rlike.container.OrdDict objects
    # R data frames --> np.recarray objects
    # The rest are returned as RPy2.robjects objects the same way as if NumPy weren't installed.
    # I could not figure out how to override this behaviour.
    # Hence the NumPy --> 'standard Python' conversion routines.
    
    @staticmethod
    def _pythonize(rval):
        """
        Takes an object returned by RPy2 and converts it to "standard" Python objects.
        This is necessary because I don't want to show RPy2 and/or NumPy objects
        to the students in a JupyterLab notebook.
        The elegant way would have been to write converters for RPy2 but
        I cannot understand the relevant manual pages.
        """
        if HAS_NUMPY:
            # RPy2 behaves differently if NumPy is installed.
            # R vectors are converted to NumPy arrays, except logical vectors (!)
            # Lists are converted to rpy2.rlike.container.OrdDict objects (mostly)
            return RHarness._numpy_pythonize(rval)
        else:
            # Without NumPy, RPy2 returns RPy2 objects quite predictably
            return RHarness._rpy2_pythonize(rval)
    
    @staticmethod
    def _numpy_pythonize(rval):
        """
        Converts the NumPy object returned by RPy2 to a "standard" Python object.
        Takes care of NumPy arrays, everything else is sent to _rpy2_pythonize
        :param rval: A NumPy object
        :returns: A corresponding Python object.
        """
        if isinstance(rval, np.recarray):
            # R data frames. Order is important
            # because `np.recarray` is an `np.ndarray`
            return RHarness._numpy_recarray_to_pydictlist(rval)
        elif isinstance(rval, np.ndarray):
            # R vectors, matrices, multidimensional arrays
            return RHarness._numpy_array_to_pylist(rval)
        else:
            # Some R objects still end up converted to RPy2 objects,
            # for instance logical vectors, and lists which end up as OrdDict objects,
            # plus all the rest...
            return RHarness._rpy2_pythonize(rval)
        
    @staticmethod
    def _numpy_array_to_pylist(arr):
        """
        Converts a NumPy array to a Python list.
        Multidimensional arrays will be converted to lists of lists as appropriate.
        :param arr: a NumPy array
        :returns: A Python list, or a POD if the array had a single element only
        """
        if arr.ndim > 1:
            # convert to lists of lists recursively
            retlist = [ RHarness._numpy_array_to_pylist(x) for x in list(arr) ]
            return retlist
        
        # handle one-dimensional arrays (end of recursion)
        # Boolean arrays are integers inside
        if np.issubdtype(arr.dtype, np.bool_):
            retlist = list(map(bool, arr))
        else:
            retlist = list(arr)
        return retlist[0] if len(retlist) == 1 else retlist
    
    @staticmethod
    def _numpy_recarray_to_pydictlist(ra):
        """
        Converts a NumPy recarray to a Python dictionary of lists,
        because R data frames are returned as np.recarray _if_ NumPy is installed.
        :param ra: A NumPy recarray, hopefully representing an R data frame.
        :returns: A Python dictionary of lists (column vectors of the data frame)
        """
        # The "column names", thanks to `https://stackoverflow.com/a/8530814`
        colnames = ra.dtype.names
        dl = { colname: RHarness._numpy_array_to_pylist(ra[colname]) for colname in colnames}
        return dl
        
    # -- rpy2.robjects conversions --
    
    @staticmethod
    def _rpy2_pythonize(rval):
        """
        Converts the object returned by RPy2 to a "standard" Python object.
        :param rval: An RPy2 object
        :returns: A corresponding, reasonably Pythonized object.
        """
        if isinstance(rval, rpy2.robjects.DataFrame):
            # convert to Python dictionary of lists
            return RHarness._rdataframe_to_pydictlist(rval)
        elif isinstance(rval, rpy2.robjects.vectors.Matrix):
            # convert to Python list of lists [[row1],[row2],...]
            return RHarness._rmatrix_to_pylist(rval)
        elif isinstance(rval, rpy2.robjects.vectors.Array):
            # general n-dimensional array type, subtype of Vector so it must come first
            return RHarness._rarray_to_pylist(rval)
        elif isinstance(rval, (rpy2.robjects.vectors.Vector, rpy2.rlike.container.OrdDict)):
            # R vectors and lists, including "simple variables" which are one-element vectors
            # convert them to Python dicts (possibly recursive), lists or a single variable
            return RHarness._rlistvec_to_pydictlist(rval)
        else:
            # let's hope we can deal with it as it is...
            return rval
        
    @staticmethod
    def _rlistvec_to_pydictlist(rlistvec):
        """
        Converts an R list or vector to a Python dictionary or list recursively.
        :param rlistvec: An instance of rpy2.robjects.vectors.Vector (which includes R lists, i.e. ListVector-s in Rpy2)
        :returns: the corresponding list of lists
        """
        retlist = None
        if isinstance(rlistvec, rpy2.robjects.vectors.ListVector):
            # R list. Convert it to a dictionary. The values are obtained with
            # the `rx2` method, this corresponds to the `$` or `[[` operator in R.
            # Note that `ListVector` is a subclass of `Vector`
            # that's why this `if` comes first
            pdict = { name : RHarness._pythonize(rlistvec.rx2(name)) for name in rlistvec.names }
            return pdict
        elif isinstance(rlistvec, rpy2.rlike.container.OrdDict):
            # When NumPy is available then lists are converted to this type instead.
            # They understand the standard Python dict syntax
            pdict = { key: RHarness._pythonize(rlistvec[key]) for key in rlistvec }
            return pdict
        # Specialised Vector-s. I have no idea whether this element-wise conversion
        # is necessary, the docs are unclear (to me) on the subject
        elif isinstance(rlistvec, rpy2.robjects.vectors.BoolVector):
            retlist = [ bool(x) for x in rlistvec ]
        elif isinstance(rlistvec, rpy2.robjects.vectors.IntVector):
            retlist = [ int(x) for x in rlistvec ]
        elif isinstance(rlistvec, rpy2.robjects.vectors.FloatVector):
            retlist = [ float(x) for x in rlistvec ]
        elif isinstance(rlistvec, rpy2.robjects.vectors.StrVector):
            retlist = [ str(x) for x in rlistvec ]
        elif isinstance(rlistvec, rpy2.robjects.vectors.Vector):
            # good luck...
            retlist = list(rlistvec)
        else:
            return None
        return retlist[0] if len(retlist) == 1 else retlist

    @staticmethod
    def _rmatrix_to_pylist(rmat):
        """
        Converts an R matrix to a Python list of lists
        :param rmat: An instance of rpy2.robjects.vectors.Matrix
        :returns: A Python list of lists, the 2nd level corresponds to the rows
        """
        pymat = [ list(rmat.rx(rowidx, True)) for rowidx in range(1, rmat.nrow+1) ]
        return pymat

    @staticmethod
    def _rarray_to_pylist(rarr):
        """
        Converts an R array to a Python list-of-lists-of-lists-... as appropriate for the 
        number of dimensions.
        :param rarr: An instance of rpy2.robjects.vectors.Array
        :returns: Python lists of lists ... etc.
        """
        try:
            # only n>1 - dimensional arrays have a `dim` attribute
            dims = list(map(int, rarr.dim))
            # To extract along the first dim, the rest of the indices must be True
            trues = (len(dims)-1) * [True]
            # recurse
            # using `rarr.rx(idx, True, True, ...)` as appropriate
            return [ RHarness._rarray_to_pylist(rarr.rx(*([idx]+trues))) for idx in range(1, dims[0]+1) ]
        except AttributeError:
            # end recursion: 1-D vectors have no `dim` attribute
            return RHarness._rlistvec_to_pydictlist(rarr)
    
    @staticmethod
    def _rdataframe_to_pydictlist(rdf):
        """
        Converts an R data frame to a Python dictionary of lists (=columns of the data frame).
        :param rdf: An instance of rpy2.robjects.DataFrame
        :returns: A Python dictionary. The keys are the data frame column names,
            the values are the column vectors as lists.
        """
        pydl = { colnm:list(rdf.rx2(colnm)) for colnm in rdf.colnames }
        return pydl

