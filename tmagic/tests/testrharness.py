"""
Unit tests for the 'RHarness' class.
NOTE: RHarness needs IPython
Run this test with `ipython -m unittest tmagic.tests.testrharness`
"""

### NOTE: RPy2 converts R objects to Python objects differently
### depending on whether NumPy is installed or not.
### The TestMagic virtual environment as set up by `devsetup.sh`
### does NOT install NumPy as it is not needed.
### You should run these unit tests also with NumPy installed in the virtualenv
### just to make sure the NumPy array, recarray, OrdDict etc. conversions also work.

import unittest
from textwrap import dedent

from ..ipython import IPYTHON
from ..r.harness import RHarness

@unittest.skipIf(not IPYTHON.is_avail, "No IPython, no RHarness testing")
class TestRHarness(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._LASTEXPR = dedent("""\
        a <- 6
        b <- 7
        # 42.0 (double, not integer)
        a*b
        """)

        cls._NORETVAL = dedent("""\
        # bogus function definition
        fn <- function() {
            return(NULL)
        }
        fn()
        """)
        
        cls._INTMATRIX = "matrix(as.integer(c(11,12,13, 21,22,23)), nrow=2, byrow=T)"

    def setUp(self):
        self.harness = RHarness()

    def test_emptycode(self):
        self.assertIsNone(self.harness._exec_script("  \n \t "))

    def test_rlines_to_charvec(self):
        self.assertIsNone(RHarness._rlines_to_charvec("  "))
        self.assertEqual(r'"\"Joe\""', RHarness._rlines_to_charvec('"Joe"'))
        self.assertEqual(
            'c("a <- 6","b <- 7","# 42.0 (double, not integer)","a*b")',
            RHarness._rlines_to_charvec(self._LASTEXPR)
        )
    
    def test_space(self):
        # Compensating for an RPy2 3.5.10 bug when running the %R one-liner magic
        self.assertEqual("\"colMeans(wh[,-3])\"", RHarness._rlines_to_charvec("colMeans(wh[, -3])"))
        # make a small df
        code = dedent("""\
        wh <- data.frame(foo=c(1,1), bar=c(2,2), fred=c(3,3))
        colMeans(wh[  , -3])
        """)
        self.assertListEqual([1.0, 2.0], self.harness._exec_script(code))

    def test_rvector_to_pylist(self):
        # single-element result
        res1 = self.harness._exec_script('6.0 * 7.0')
        self.assertEqual(42.0, res1)
        # vector result
        res2 = self.harness._exec_script("tabulate(c(1,2,2,3,4,4,4))")
        self.assertListEqual([1, 2, 1, 3], res2)
        # various vector types
        rb = self.harness._exec_script("c(F,T)")
        self.assertListEqual([False, True], rb)
        ri = self.harness._exec_script("c(3,6,9)")
        self.assertListEqual([3,6,9], ri)
        rf = self.harness._exec_script("c(2.71, 3.14)")
        self.assertListEqual([2.71, 3.14], rf)
        rs = self.harness._exec_script("c('Joe', 'Mary')")
        self.assertListEqual(["Joe", "Mary"], rs)
        # complicated list of vectors
        rlv = self.harness._exec_script('list(foo=c(3,5), bar=c("joe","ann","mary"), singleval=3.14)')
        self.assertDictEqual({"foo":[3.0, 5.0], "bar":["joe","ann","mary"], "singleval":3.14}, rlv)

    def test_rmatrix_to_pylist(self):
        # we test only the numerical matrices
        # integer matrix
        rim = self.harness._exec_script(self._INTMATRIX)
        self.assertListEqual([[11,12,13], [21,22,23]], rim)
        # 3-D float array
        rfm = self.harness._exec_script("array(1:24, dim=c(3,2,4))")
        self.assertListEqual([
            [[1, 7, 13, 19], [4, 10, 16, 22]],
            [[2, 8, 14, 20], [5, 11, 17, 23]],
            [[3, 9, 15, 21], [6, 12, 18, 24]],
        ], rfm)
    
    def test_rdataframe_to_pylist(self):
        rdf = self.harness._exec_script('data.frame(person=c("Joe","Ann","Mary"), age=c(21,19,32))')
        self.assertDictEqual({"person":["Joe","Ann","Mary"], "age":[21.0,19.0,32.0]}, rdf)
    
    def test_exec_script(self):
        # just define a function, then invoke it, should return NULL
        retval = self.harness._exec_script(self._NORETVAL)
        self.assertIsNone(retval)
        retval = self.harness._exec_script(self._LASTEXPR)
        self.assertEqual(42.0, retval)

    def test_singlevar_runtest(self):
        ok = self.harness.test_expr(42.0, self._LASTEXPR)
        self.assertTrue(ok)
        self.assertEqual(self.harness.last, 42.0)

    def test_vector_runtest(self):
        # try the various basic vector types
        self._runtest_compare([False, True], "c(F, T)") # bool
        self._runtest_compare([3,5,7,9], "seq(from=3, to=9, by=2)") # int
        self._runtest_compare([3.14, 2.71], "c(3.14, 2.71)")    # float
        self._runtest_compare(["joe", "mary"], "c('joe', 'mary')")  # str

    def test_rlist_runtest(self):
        self._runtest_compare({"head":1.0, "tail":{"due":2.0, "last":[3.0,4.0]}},
                              "list(head=1, tail=list(due=2, last=c(3,4)))")

    def test_rmat_runtest(self):
        # just with the int matrix...
        self._runtest_compare([[11,12,13], [21,22,23]], self._INTMATRIX)

    def test_save_state(self):
        OURLIST = [27, 64]
        # get this variable in the global environment
        self.harness._exec_script("x <- c(27, 64)")
        # see if it's there
        self.assertListEqual(OURLIST, self.harness._exec_script("x"), msg="_exec_script should see 'x'")
        # ...and if we check for it?
        self.assertTrue(self.harness.test_expr(OURLIST, "x"), msg=f"test_expr {OURLIST} == 'x' should eval to True")
        # we expect the user to append 45 to it
        explst = [27, 64, 45]
        # but she deleted the 2nd element by mistake
        self.assertFalse(self.harness.test_expr(explst, "x <- x[-2];x"))
        # `x` should have stayed intact in the global interpreter state
        self.assertListEqual(OURLIST, self.harness._exec_script("x"))
        # user corrects the mistake
        self.assertTrue(self.harness.test_expr(explst, "x <- c(x, 45); x"))
        # ...and lst must be updated globally
        self.assertListEqual(explst, self.harness._exec_script("x"))
    
    # -- "hidden" methods --

    def _runtest_compare(self, expval, rcode):
        """
        Compares the expected value to the result of the R commands in `rcode` with `RHarness.test_expr()`
        and checks if it works
        :param expval: The expected return value of something run in R
        :param rcode: The R code as a string that's supposed to be tested
        """
        ok = self.harness.test_expr(expval, rcode)
        self.assertTrue(ok)
        if isinstance(expval, dict):
            self.assertDictEqual(self.harness.last, expval)
        elif isinstance(expval, list):
            self.assertListEqual(self.harness.last, expval)
        else:
            self.assertEqual(self.harness.last, expval)

# == MAIN ==

if __name__ == "__main__":
    unittest.main()
