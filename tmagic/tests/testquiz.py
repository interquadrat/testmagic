"""
Tests for the `quiz` module.
"""

import unittest
import tempfile
import os

from ..quiz import *
from ..secret import JSONCrypt

# module-wide "constants" needed by more than one test class
MYPATH = os.path.dirname(os.path.abspath(__file__))
BADANS = Answer("Wrong answer")
GOODANS = Answer("Good answer", correct=True)
GOODQUESTION = Question("Good or bad?", [GOODANS, BADANS])

class TestAnswer(unittest.TestCase):
    def test_defaults(self):
        # default wrong answer
        self.assertEqual("Wrong answer", BADANS.answer)
        self.assertFalse(BADANS.correct)
        self.assertEqual("False, please try again", BADANS.feedback)
        
        # default good answer
        self.assertEqual("Good answer", GOODANS.answer)
        self.assertTrue(GOODANS.correct)
        self.assertEqual("Correct", GOODANS.feedback)

    def test_dict(self):
        # init
        ANSDIR = {"answer": "Good answer", "correct": True, "feedback": "Correct"}
        a = Answer(ANSDIR)
        self.assertDictEqual(ANSDIR, a.to_dict)
        
        # overwrite
        b = Answer("Some question")
        diag = b.from_dict(ANSDIR)
        self.assertEqual("OK", diag)
        self.assertDictEqual(ANSDIR, b.to_dict)
        
        # bad dict (not all possible problems are checked)
        diag = BADANS.from_dict({"answer": "very bad!"})    # keys missing
        self.assertEqual("ERROR: 'correct'", diag)
        self.assertEqual("Wrong answer", BADANS.answer)     # no change
    
    def test_feedback(self):
        a = Answer("Right answer", correct=True, feedback="Well done")
        self.assertEqual("Well done", a.feedback)
        # set to bad, with default feedback
        a.set_correct(False)
        self.assertFalse(a.correct)
        self.assertEqual("False, please try again", a.feedback)
        # set to good, with default feedback
        a.set_correct(True)
        self.assertTrue(a.correct)
        self.assertEqual("Correct", a.feedback)


class TestQuestion(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.quizfnm = os.path.join(MYPATH, "question.json")
        
    def setUp(self):
        pass

    def test_str_init(self):
        # no answer
        qu = Question("No answer")
        self.assertFalse(qu.has_correct_answer)
        
        # wrong answer type
        with self.assertRaises(ValueError) as errctx:
            qx = Question("Bad answer type", "Can't be a string")
        self.assertEqual("Question(...,answers): answers must be None, Answer or list of Answers", 
            str(errctx.exception))
        
        with self.assertRaises(ValueError) as errctx:
            qu.add_answer("Can't be a string")
        self.assertEqual("Question.add_answer: expects Answer instance", str(errctx.exception))
        
        self.assertTrue(GOODQUESTION.has_correct_answer)
        
        # implicitly tests passing a single Answer instance to ctor
        qbad = Question("Bad", BADANS)
        self.assertFalse(qbad.has_correct_answer)
        # now add the good answer
        qbad.add_answer(GOODANS)
        self.assertTrue(qbad.has_correct_answer)
    
    def test_dict_init(self):
        # only a "good input" is tested
        with open(self.quizfnm) as qf:
            qdict = json.load(qf)
        qd = Question(qdict)
        self.assertDictEqual(qd.to_dict, GOODQUESTION.to_dict)
    
    def test_conversion(self):
        with open(self.quizfnm) as qf:
            quizstr = qf.read()
        self.assertMultiLineEqual(quizstr, json.dumps(GOODQUESTION.to_dict, indent=4))


class TestQuiz(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # temp dir for JSON encryption
        cls.TEMPDIR = tempfile.TemporaryDirectory()
        cls.KEYFNM = os.path.join(cls.TEMPDIR.name, "key")
        cls.ENCJSON = os.path.join(cls.TEMPDIR.name, "secret.json")

        # encrypted JSON
        jc = JSONCrypt()
        jc.make_key(cls.KEYFNM)
        cls.QUIZDICT = { "test": GOODQUESTION.to_dict }
        jc.encrypt_jsonfile(cls.QUIZDICT, cls.ENCJSON)

    def setUp(self):
        pass

    def test_questions_from_dict(self):
        quiz = Quiz(self.QUIZDICT)
        self._check(quiz)

    def test_questions_from_plaintext_jsonfile(self):
        quiz = Quiz(os.path.join(MYPATH, "quiz.json"))
        self._check(quiz)

    def test_questions_from_encrypted_jsonfile(self):
        quiz = Quiz(self.ENCJSON, self.KEYFNM)
        self._check(quiz)

    def _check(self, quiz):
        qq = quiz.questions
        self.assertDictEqual(GOODQUESTION.to_dict, qq["test"].to_dict)

    def test_to_dict(self):
        quiz = Quiz(self.QUIZDICT)
        self.assertDictEqual(self.QUIZDICT, quiz.to_dict)

    @classmethod
    def tearDownClass(cls):
        cls.TEMPDIR.cleanup()


# == MAIN ==

if __name__ == "__main__":
    unittest.main()
