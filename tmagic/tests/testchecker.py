"""
Unit tests for the 'checker'module.
"""

import unittest

from ..checker import AlmostEqual

class TestAlmostEqual(unittest.TestCase):
    def setUp(self):
        self.aeq = AlmostEqual()    # default relative tolerance

    def test_simples(self):
        # "good" and "bad" values for simple variables
        vals = (
            (None, 1),
            (True, False),
            (1, 2),
            (3.14, 3.15),
            ("foo", "bar"),
            (1, "fred"),
        )
        # test each
        for good, bad in vals:
            self.assertTrue(self.aeq.compare(good, good))
            self.assertFalse(self.aeq.compare(good, bad))

    def test_floats(self):
        eps = 1e-10
        self.assertTrue(self.aeq.compare(1.0, 1.0 + eps))
        # change the tolerance
        eps = 1e-5
        taeq = AlmostEqual(eps)
        self.assertTrue(taeq.compare(1.0, 1.0 + 0.1*eps))
        self.assertFalse(taeq.compare(1.0, 1.0 + 10.0*eps))
        # float lists
        x1 = [3.14, 1.0, 2.71]
        x2 =x1.copy()
        x2[1] += 0.1*eps
        self.assertTrue(taeq.compare(x1, x2))
        x2[1] += 9.9*eps
        self.assertFalse(taeq.compare(x1, x2))
        
    def test_composites(self):
        # "good" and "bad" values for various composite data structures
        vals = (
            ({'uno':1, 'lst': [1,2,3], 'due':2}, {'dos':2, 'lst': [1,3], 'tres':3}, ), # dict
            ([27, [3.14, 2.71], 64], [64, 33.3, 27.3], ),  # list
            (('foo', (64, 'bar'), 'moo'), (('foo','moo'), ('fred',27)), ),  # tuple
            (set(('foo','bar')), set(('foo','barbar')), ),
        )
        # test each
        for good, bad in vals:
            self.assertTrue(self.aeq.compare(good, good))
            self.assertFalse(self.aeq.compare(good, bad))

# == MAIN ==

if __name__ == "__main__":
    unittest.main()
