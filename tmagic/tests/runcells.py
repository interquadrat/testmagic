#!/usr/bin/env ipython

"""
Test script that emulates how the %%pyexer magic runs in an IPython cell
(and later in a JupyterLab notebook cell).
Run this from the top-level `testmagic` directory to get the import path right.
:author: András Aszódi
:date: 2023-09-01
"""

from textwrap import dedent

from IPython.core.getipython import get_ipython

from tmagic.exercise import ExerMagic

# == MAIN ==

# -- consts --

NL = ["Joe", "Ann", "Bob"]
INSL = ["Pat", "Fred", "Jane"]
INSRES = NL.copy()
INSRES[0:2] = INSL
assert INSRES == ["Pat", "Fred", "Jane", "Bob"]

# Create the exercise object
em = ExerMagic(tests={"ins":INSRES})

# Run the steps in IPython
ipy = get_ipython()

# Input list
ipy.run_cell(f"nl = {NL}")

# Test with correct result
ipy.run_cell(dedent(f"""\
    %%pyexer ins
    nl[0:2] = {INSL}
    nl
    """)
)

# what is now in the global context?
assert nl == INSRES
