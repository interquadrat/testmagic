"""
Unit tests for the 'ExerMagic' class.
Because we need an active IPython interpreter,
this test must be run from the command line as follows:

`ipython -m unittest tmagic.tests.testexermagic`

The usual invocation `python3 -m unittest tmagic.tests.testexermagic` will fail.
"""

import unittest
import tempfile
import json
import os

from ..ipython import IPYTHON
from ..secret import JSONCrypt
from ..exercise import ExerMagic

@unittest.skipUnless(IPYTHON.is_avail, "Needs IPython to run")
class TestExerMagic(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # test-expected value pairs
        cls.TESTS = { "answer": 42, "square": 4, "cube": 8 }

        # Temporary directory holding the files
        cls.TEMPDIR = tempfile.TemporaryDirectory()
        cls.KEYFNM = os.path.join(cls.TEMPDIR.name, "key")
        cls.PLAINJSON = os.path.join(cls.TEMPDIR.name, "plain.json")
        cls.ENCJSON = os.path.join(cls.TEMPDIR.name, "secret.json")
        
        # plaintext JSON
        with open(cls.PLAINJSON, "w") as outf:
            json.dump(cls.TESTS, outf)
        # "encrypted" (rather obfuscated) JSON
        jc = JSONCrypt()
        jc.make_key(cls.KEYFNM)
        jc.encrypt_jsonfile(cls.TESTS, cls.ENCJSON)
        
    def setUp(self):
        self.mgc = ExerMagic()

    def test_init(self):
        self.assertDictEqual({}, self.mgc._tests)

    def test_register_1(self):
        self.mgc.register_test("answer", 42)
        self.assertDictEqual({"answer": 42}, self.mgc._tests)

    def test_pyexer(self):
        self.mgc.register_test("answer", 42)
        # this code would come from a notebook cell
        cell = "a = 2*3\nb = 3+4\na*b\n"
        self.mgc.pyexer("answer", cell)
        self.assertEqual(42, self.mgc._last)
    
    def test_log(self):
        lf = os.path.join(self.TEMPDIR.name, "test.log")
        ex = ExerMagic(tests=self.TESTS, logfile=lf)
        with self.assertLogs() as logctx:
            ex.pyexer("answer", "6*7\n")    # will pass
            ex.pyexer("square", "2+3\n")    # will fail
        self.assertEqual(logctx.output,
            ["INFO:root:answer passed", "INFO:root:square failed"])
    
    def _nreg(self, tests, keyfile=None):
        """utility function to test ExerMagic.__init__ and register_tests together"""
        # invoke `register_tests` on empty `ExerMagic` object
        self.mgc.register_tests(tests, keyfile)
        self.assertDictEqual(self.TESTS, self.mgc._tests)
        # invoke through the ctor
        em = ExerMagic(tests, keyfile)
        self.assertDictEqual(self.TESTS, em._tests)
        
    def test_register_n(self):
        # from a dictionary object
        self._nreg(self.TESTS)
        # from a plaintext JSON
        self._nreg(self.PLAINJSON)
        # from an "encrypted" JSON
        self._nreg(self.ENCJSON, self.KEYFNM)

    @classmethod
    def tearDownClass(cls):
        cls.TEMPDIR.cleanup()


# == MAIN ==

if __name__ == "__main__":
    unittest.main()
