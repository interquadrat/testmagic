"""
Unit tests for the 'RExerMagic' class.
Because we need an active IPython interpreter,
this test must be run from the command line as follows:

`ipython -m unittest tmagic.tests.testrexermagic`

The usual invocation `python3 -m unittest tmagic.tests.testrexermagic` will fail.
"""

import unittest

from ..ipython import IPYTHON
from ..secret import JSONCrypt
from ..r.exercise import RExerMagic

@unittest.skipUnless(IPYTHON.is_avail, "Needs IPython to run")
class TestRExerMagic(unittest.TestCase):
        
    def setUp(self):
        self.mgc = RExerMagic()

    def test_init(self):
        self.assertDictEqual({}, self.mgc._tests)

    def test_register_1(self):
        self.mgc.register_test("answer", 42)
        self.assertDictEqual({"answer": 42}, self.mgc._tests)

    def test_rexer(self):
        self.mgc.register_test("answer", 42)
        # this code would come from a notebook cell
        cell = "a = 2*3\nb = 3+4\na*b\n"
        self.mgc.rexer("answer", cell)
        self.assertEqual(42, self.mgc._last)


# == MAIN ==

if __name__ == "__main__":
    unittest.main()
