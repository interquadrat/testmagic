"""
Discovers and runs unit tests.
Note that this works only with an IPython interpreter.
Invoke from the project root like `ipython tmagic/tests/runtests.py`.
Inspired by https://stackoverflow.com/questions/644821
/python-how-to-run-unittest-main-for-all-source-files-in-a-subdirectory
("p4sven"-s solution).

:author: András Aszódi
:date: 2022-11-15
"""

import unittest
import sys

# -- Functions --

# The `TestLoader().discover()` method can discover only tests that are
# in packages immediately below the starting directory.
def test_runner(subdir, title="testmagic"):
    """
    Discovers and runs unit tests under a given subdirectory.
    :param subdir: The path to the subdirectory, relative to 
        the directory where this script lives.
    :param title: A string that is printed as "Running $title unit tests"
    :return: True if all tests passed, False otherwise.
    """
    # note: pattern is the default, just spelling it out for reference
    testsuite = unittest.TestLoader().discover(subdir, pattern = "test*.py")
    print(f"*** Running {title} unit tests ***")
    return unittest.TextTestRunner(verbosity=1).run(testsuite).wasSuccessful()

# == MAIN ==

def main():
    """
    The main function. Needed by `pyproject.toml`
    """
    ok = test_runner(".")
    sys.exit(0 if ok else 1)

if __name__ == "__main__":
    main()
