"""
Unit tests for the 'PyHarness' class.
Implicitly this tests the 'Harness' abstract base class, too.
"""

import unittest
from textwrap import dedent
import builtins

from ..pyharness import PyHarness
from ..ipython import IPYTHON

class TestPyHarness(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._LASTEXPR = dedent("""\
        a = 6
        b = 7
        # 42 should be returned
        a*b
        """)

        cls._NORETVAL = dedent("""\
        # bogus function
        def fn():
            pass
        fn()
        """)

    def setUp(self):
        self.harness = PyHarness()
    
    def test_emptycode(self):
        self.assertIsNone(self.harness._exec_script("  \n \t "))

    def test_lastexpr(self):
        retval = self.harness._exec_script(self._LASTEXPR)
        self.assertEqual(42, retval)
        # create a second PyHarness, see if it remembers `a` and `b`
        # which were defined in the previous script
        harness2 = PyHarness()
        retval2 = harness2._exec_script("a + b")
        self.assertEqual(13, retval2)

    def test_code(self):
        retval = self.harness._exec_script(self._NORETVAL)
        self.assertIsNone(retval)

    def test_simple_runtest(self):
        ok = self.harness.test_expr(42, self._LASTEXPR)
        self.assertTrue(ok)

    def test_datastruct_runtest(self):
        # "good" and "bad" expression examples for various data structures
        # here we test Harness._check()
        exprs = (
            ("3**3 == 27", "3 + 2 == 32",), # bool
            ("{}", "{'not':'empty'}", ), # empty dict
            ("{'uno':1, 'due':2}", "{'dos':2, 'tres':3}", ), # dict
            ("[]","['notempty',]", ), # empty list
            ("[27, 64]", "[64, 27]", ),  # list
            ("tuple()","('notempty',)", ), # empty tuple
            ("(27, 64)", "(64, 27)", ),  # tuple
            ("set(('foo','bar'))", "set(('foo','barbar'))", ), # set
        )
        # test each "cell expression"
        for good, bad in exprs:
            expval = eval(good)
            self.assertTrue(self.harness.test_expr(expval, good), msg=expval)
            self.assertFalse(self.harness.test_expr(expval, bad), msg=expval)
    
    def test_save_state(self):
        # run a piece of code in a cell
        # `silent=True` this always returns None (IPython feature...)
        def _runcode(code):
            IPYTHON.instance.run_cell(code, silent=True)
        
        # look up the value of a variable in the user's global namespace in IPython
        def _lookup(varname):
            return PyHarness.GLOBAL_CONTEXT[varname]
        
        OURLIST = [27, 64]
        # get this variable in the user's context
        _runcode(f"lst = {OURLIST}")
        # we expect the user to append 45 to it
        explst = OURLIST.copy()
        explst.append(45)
        # but she deleted the 2nd element instead
        self.assertFalse(self.harness.test_expr(explst, "del lst[1]; lst"))
        # `lst` should have stayed intact in the global interpreter state
        self.assertListEqual(OURLIST, _lookup("lst"))
        # user corrects the mistake
        self.assertTrue(self.harness.test_expr(explst, "lst.append(45); lst"))
        # ...and lst must be updated globally
        self.assertListEqual(explst, _lookup("lst"))
        
        # a PyLang example that strangely went wrong in the selflearn notebook
        # even if the user solves the exercises correctly
        # (code is executed twice)
        # However, this test completes OK in IPython, so it's an IPykernel issue.
        NAMES = ("Joe", "Ann", "Bob")  # note that this is a tuple
        _runcode(f"names = {NAMES}")
        _runcode("nl = list(names)")        # make it a list, kinda deep copy if you want
        self.assertListEqual(["Joe","Ann","Bob"], _lookup("nl"))
        self.assertTrue(self.harness.test_expr(["Joe","Ann","Eve"], 'nl[2] = "Eve"; nl'))
        # the replacement that was strange
        expct = ["Pat", "Fred", "Jane", "Eve"]
        self.assertTrue(self.harness.test_expr(expct, 'nl[0:2] = ["Pat", "Fred", "Jane"]; nl'))
        self.assertListEqual(expct, _lookup("nl"))
        
        

# == MAIN ==

if __name__ == "__main__":
    unittest.main()
