"""
Tests the 'secret' module.
:author: András Aszódi
:date: 2023-01-20
"""

import unittest
import tempfile
import os.path
import json

from ..secret import FileCrypt, JSONCrypt

class TestFileCrypt(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.TEMPDIR = tempfile.TemporaryDirectory()
        cls.DATA = "My very secret data\non two lines"
        cls.KEYFNM = os.path.join(cls.TEMPDIR.name, "key")
        cls.ENCFNM = os.path.join(cls.TEMPDIR.name, "secret.dat")
    
    def setUp(self):
        self.fc = FileCrypt()
        # create and save the key
        self.fc.make_key(self.KEYFNM)
    
    def test_write_read(self):
        self.fc.write_data(self.DATA, self.ENCFNM)
        
        # create a new FileCrypt object, read the key into it and then decrypt
        fc2 = FileCrypt(self.KEYFNM)
        plaintext = fc2.read_data(self.ENCFNM)
        self.assertMultiLineEqual(self.DATA, plaintext)
    
    @classmethod
    def tearDownClass(cls):
        cls.TEMPDIR.cleanup()


class TestJSONCrypt(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.TEMPDIR = tempfile.TemporaryDirectory()
        cls.OBJ = ['foo', {'bar': ('baz', None, 1.0, 2)}]   # canonical JSON example from the Python docs
        cls.JSON = json.dumps(cls.OBJ)  # string, easier to compare
        cls.KEYFNM = os.path.join(cls.TEMPDIR.name, "key")
        cls.ENCFNM = os.path.join(cls.TEMPDIR.name, "secret.json")
    
    def setUp(self):
        self.jc = JSONCrypt()
        # create and save the key
        self.jc.make_key(self.KEYFNM)
    
    def test_write_read(self):
        self.jc.encrypt_jsonfile(self.OBJ, self.ENCFNM)
        
        # create a new JSONCrypt object, read the key into it and then decrypt
        jc2 = JSONCrypt(self.KEYFNM)
        obj = jc2.jsonfile_decrypt(self.ENCFNM)
        self.assertEqual(self.JSON, json.dumps(obj))
    
    @classmethod
    def tearDownClass(cls):
        cls.TEMPDIR.cleanup()

# == MAIN ==

if __name__ == "__main__":
    unittest.main()
