"""
Unit tests for the `ipython` module. Extremely simple-minded.
Run this test with `ipython -m unittest tmagic.tests.testipython`
"""

import unittest

from ..ipython import IPYTHON

@unittest.skipIf(not IPYTHON.is_avail, "No IPython, no testing")
class MyTestCase(unittest.TestCase):
    def test_ipython_avail(self):
        self.assertTrue(IPYTHON.is_avail)

    def test_ipython_instance(self):
        self.assertIsNotNone(IPYTHON.instance)


if __name__ == '__main__':
    unittest.main()
