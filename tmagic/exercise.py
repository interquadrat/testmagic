"""
This module provides a base class for running Python exercises in an IPython notebook cell.
:author: András Aszódi
:date: 2020-11-03
"""

# Implementation note: the class is based on the IPython manual
# See https://ipython.readthedocs.io/en/stable/config/custommagics.html

from sys import stderr
import json
import logging

from IPython.core.magic import Magics, magics_class, cell_magic

from .ipython import IPYTHON
from .checker import DEFAULT_TOL
from .pyharness import PyHarness
from .secret import JSONCrypt

# -- Classes --

@magics_class
class ExerMagic(Magics):
    """
    Defines magics to run a piece of code wrapped in a unit test harness.
    """

    def __init__(self, tests=None, keyfile=None, rel_tol=DEFAULT_TOL, logfile=None):
        """
        Initialiser. Registers the created instance with IPython.
        :param tests: Either a dictionary with test name - expected value pairs,
            or the name of a JSON file from which such a dictionary can be `load`-ed.
            If None or omitted then no tests are registered (this is the default).
        :param keyfile: If not the default (`None`), then this string is supposed
            to be an encryption key file with which the contents of
            the `tests` obfuscated JSON file can be decrypted.
        :param rel_tol: The relative tolerance to be used in comparing floats.
        :param logfile: If specified, test success/failure will be logged to this file.
            If not specified (None), no logging is performed.
        """
        # Set up this TestMagic instance as an IPython magic
        super().__init__(None)
        IPYTHON.instance.register_magics(self)

        self._reltol = rel_tol
        
        # Test names and expected values
        self._tests = {}
        if tests is not None:
            self.register_tests(tests, keyfile)
        
        # remember the last result
        # this is mainly for testing/debugging purposes
        self._last = None
        
        # logging
        if logfile is not None:
            logging.basicConfig(filename=logfile, format="%(levelname)s:%(message)s",
                level=logging.INFO, force=True)
            self._log = True
        else:
            self._log = False

    def register_test(self, testname, expvalue):
        """
        Registers a single test (stores the expected value)
        :param testname: The name of the test
        :param expvalue: The expected value coming from the test
        """
        self._tests[testname] = expvalue

    def register_tests(self, tests, keyfile=None):
        """
        Convenience method to register many tests at once.
        Invokes the `register_test()` method on all key-value pairs provided in `tests`
        :param tests: Either a dictionary with test name - expected value pairs,
            or the name of a JSON file from which such a dictionary can be `load`-ed.
        :param keyfile: If not the default (`None`), then this string is supposed
            to be an encryption key file with which the contents of
            the `tests` obfuscated JSON file can be decrypted.
        Errors are swallowed but a warning is printed.
        """
        try:
            if keyfile is not None:
                # assume `tests` is an encrypted JSON file
                jc = JSONCrypt(keyfile)
                data = jc.jsonfile_decrypt(tests)
            elif isinstance(tests, str):
                with open(tests) as inf:
                    data = json.load(inf)
            elif isinstance(tests, dict):
                data = tests
            else:
                raise ValueError("'tests' argument must be a filename or a dictionary")
            self._tests.update(data)
        except Exception as err:
            print(f"ERROR: {str(err)} in 'register_tests', ignored", file=stderr)

    @cell_magic
    def pyexer(self, testname, cell):
        """
        IPython 'cell magic' to wrap a piece of Python code (sequence of expressions)
        in a unit test harness and have it run.
        Usage within an IPython cell:
        +-------------------------+
        | %%pyexer testname       |
        | Python code line        |
        | ... more code lines ... |
        +-------------------------+

        :param testname: Identifies the test to be used.
        :param cell: Contents of the cell
        """
        # Create a PyHarness instance to test the cell's contents
        harness = PyHarness(self._reltol)
        self._run_exercise(harness, testname, cell)
        self._last = harness.last
        return None     # to suppress printing in a Notebook
        
    # -- "Private" methods --

    def _run_exercise(self, harness, testname, cell):
        """
        Runs the exercise in a "test harness".
        :param harness: The test harness that knows how to run Python or R code.
        :param testname: The name of the test. The corresponding expected value
            will be looked up within the method and compared to what the harness returned.
        :param cell: The contents of an IPython cell, i.e. the code to be executed.
        :return: The result of the last line of the `cell` code.
        """
        ok = harness.test_expr(self._tests[testname], cell)
        if ok:
            print("Passed :-)")
        else:
            # JupyterLab will print this with a red background
            print("Failed :-(", file=stderr)
        self._log_exercise(testname, ok)
        return harness.last

    def _log_exercise(self, testname, success):
        """
        If logging is enabled, write an INFO line
        stating if a given test was successfully solved or not.
        :param testname: The name of the test
        :param success: if True, the test was solved, if False than it wasn't.
        """
        if self._log:
            logging.info(f"{testname} {'passed' if success else 'failed'}")

