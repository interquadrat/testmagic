#!/usr/bin/env python3 -m IPython

### NOTE: `ipython` is an alias so `env` doesn't recognise it...

"""
This script helps you build quiz questions and answers.
:author: András Aszódi
:date: 2023-02-07
"""

import cmd
import json

from tmagic.quiz import *

# -- Classes --

class AnswerBuilder(cmd.Cmd):
    """
    Command-line interface to build a quiz answer.
    """

    # class vars
    intro = "Build an answer\n"
    prompt = "Question/Answer> "

    def __init__(self):
        """
        Creates an AnswerBuilder instance
        """
        super().__init__()
        self._answer = None

    def do_create(self, answertext):
        """
        Creates an answer. By default, it will be a "false" answer.
        The previous answer, if any, is "forgotten".
        :param answertext: The text of the answer.
        """
        self._answer = Answer(answertext)

    def do_correct(self, corr):
        """
        Sets the "correct" field to `corr`. Updates the `feedback` field accordingly.
        You may wish to run the `feedback` command afterwards if the default feedback is not appropriate.
        :param corr: Whether the answer should be correct or not: `True` or `False`
        """
        self._answer.set_correct(corr)
        print(f"Answer is set to {self._answer.correct}")

    def do_feedback(self, feedbacktext):
        """
        Sets the "feedback" field.
        :param feedbacktext: The new feedback text.
        """
        self._answer.set_feedback(feedbacktext)

    def do_show(self, _):
        """
        Prints the current answer in JSON format.
        """
        if not self.ok:
            print("Please 'create' an answer")
            return False
        print(json.dumps(self._answer.to_dict, indent=4))

    def do_return(self, _):
        """
        Return to the Question builder.
        """
        return True

    @property
    def ok(self):
        return self._answer is not None

    @property
    def answer(self):
        return self._answer


class QuestionBuilder(cmd.Cmd):
    """
    Command-line interface to build a quiz question.
    """

    # class vars
    intro = "Welcome to the Quiz question builder.\nType 'help' or '?' to see the commands\n"
    prompt = "Question> "

    def __init__(self):
        """
        Creates a QuestionBuilder instance.
        """
        super().__init__()
        self._question = None

    def do_about(self, _):
        """
        Print information about the Quiz question builder.
        """
        print("Quiz question builder (c) András Aszódi, 2023. All rights reserved.")

    def do_create(self, qtext):
        """
        Creates a new Question.
        :param qtext: The question text
        """
        self._question = Question(qtext)

    def do_answer(self, _):
        """
        Add an answer.
        The answer will be created interactively.
        """
        ab = AnswerBuilder()
        ab.cmdloop()
        if ab.ok:
            self._question.add_answer(ab.answer)
        else:
            print("Please create an answer")

    def do_show(self, _):
        """
        Prints the JSON of the current question.
        """
        if self._check():
            print(json.dumps(self._question.to_dict, indent=4))

    def do_save(self, jsonpath):
        """
        Saves the question to a JSON file if it is OK. Otherwise a warning is printed.
        :param jsonpath: Path to a new JSON file to be written.
        """
        if jsonpath is None or jsonpath.strip() == "":
            print("Please specify a valid JSON path")
            return False
        if self._check():
            try:
                with open(jsonpath, "w") as jf:
                    json.dump(self._question.to_dict, jf, indent=4)
            except Exception as err:
                print(str(err))

    def _check(self):
        """
        Checks if the question is set up properly.
        Prints error messages to stdout as a side effect.
        :return: True if the question is OK and can be saved, False if things are still missing.
        """
        if self._question is None:
            print("Please invoke 'create' to create a question")
            return False
        if not self._question.has_correct_answer:
            print("Please add a correct answer")
            return False
        return True

    def do_exit(self, _):
        """
        Exit the application.
        """
        print("Bye")
        return True


# == MAIN ==

def main():
    """
    The main function. Needed by `pyproject.toml`
    """
    qb = QuestionBuilder()
    qb.cmdloop()

if __name__ == "__main__":
    main()
