"""
The 'secret' module provides tools for file obfuscation.
:author: András Aszódi
:date: 2023-01-20
"""

import json
import argparse
import sys

# python3 -m pip install cryptography
from cryptography.fernet import Fernet

# -- Classes --

class FileCrypt:
    """
    Encrypts (obfuscates) and decrypts text files
    using Fernet encryption.
    """
    
    def __init__(self, keyfilenm=None):
        """
        Initialises a FileCrypt object.
        :param keyfilenm: If specified (not None) then
            the key is read from a file with this name.
            By default the newly-created object is "empty", has no key.
            Use the `make_key` method to create and save a new encryption key.
        """
        # the Fernet object
        self._crypto = None
        if keyfilenm is not None:
            self.read_key(keyfilenm)
    
    def make_key(self, keyfilenm):
        """
        Generates a (random) encryption key
        and saves it to a file.
        Also stores it in the calling object.
        :param keyfilenm: The name of the key file.
        """
        key = Fernet.generate_key()
        with open(keyfilenm, "wb") as keyf:
            keyf.write(key)
        self._crypto = Fernet(key)
    
    def read_key(self, keyfilenm):
        """
        Reads an encryption key from a file
        and stores it in the calling object.
        :param keyfilenm: The name of the key file.
        """
        with open(keyfilenm, "rb") as keyf:
            key = keyf.read()
            self._crypto = Fernet(key)
    
    def write_data(self, data, encfilenm):
        """
        Writes a string or bytes into an encrypted file.
        :param data: the data (string)
        :param encfilenm: The name of the encrypted data file.
        """
        encdata = self._crypto.encrypt(data.encode())
        with open(encfilenm, "wb") as encf:
            encf.write(encdata)
    
    def read_data(self, encfilenm):
        """
        Reads the data from an encrypted file
        into the calling object.
        :param encfilenm: The name of the encrypted data file.
        :returns: The decrypted contents as a string
        """
        with open(encfilenm, "rb") as encf:
            encdata = encf.read()
            return self._crypto.decrypt(encdata).decode()
    

class JSONCrypt(FileCrypt):
    """
    Adds JSON manipulating methods to FileCrypt.
    """
    
    def __init__(self, keyfilenm=None):
        """
        Initialises a JSONCrypt object.
        :param keyfilenm: If specified (not None) then
            the key is read from a file with this name.
            By default the newly-created object is "empty", has no key.
            Use the `make_key` method to create and save a new encryption key.
        """
        super().__init__(keyfilenm)
    
    def encrypt_jsonfile(self, obj, encfilenm):
        """
        Converts a Python object hierarchy to its JSON string representation,
        encrypts it and writes it to a file.
        :param obj: A Python object hierarchy
        :param encfilenm: The name of the encrypted JSON file.
        """
        data = json.dumps(obj)
        self.write_data(data, encfilenm)
    
    def jsonfile_decrypt(self, encfilenm):
        """
        Decrypts the contents of a file and converts it to a Python object hierarchy.
        :param encfilenm: an encrypted JSON file
        :returns: A Python object hierarchy
        """
        data = self.read_data(encfilenm)
        return json.loads(data)


# == MAIN ==

def _process_commandline_options():
    """
    Parses the command-line options.
    :returns: A `Namespace` structure as usual.
    """
    parser = argparse.ArgumentParser(description = "JSON file obfuscation utility",
        formatter_class=argparse.RawTextHelpFormatter)  # don't eat newlines from help strings
    
    # Mutually exclusive options, at least one of them is mandatory
    mxgrp = parser.add_mutually_exclusive_group(required=True)
    mxgrp.add_argument('-k', "--key", type=str, metavar="keyfile",
        help="Generate a new encryption key and store it in 'keyfile'")
    mxgrp.add_argument('-e', "--encrypt", nargs=3, 
        metavar=("file.json", "keyfile", "file.enc"),
        help="Encrypt 'file.json' and save it as 'file.enc'")
    mxgrp.add_argument('-d', "--decrypt", nargs=3, 
        metavar=("file.enc", "keyfile", "file.json"),
        help="Decrypt 'file.enc' and save it as 'file.json'")
    
    args = parser.parse_args()    
    return args

def main():
    """
    The main function. Encrypts/decrypts JSON files.
    """
    args = _process_commandline_options()
    try:
        if args.key:
            f = FileCrypt()
            f.make_key(args.key)
        elif args.encrypt:
            plainfnm, keyfnm, encfnm = args.encrypt
            with open(plainfnm) as inf:
                data = json.load(inf)
                jf = JSONCrypt(keyfnm)
                jf.encrypt_jsonfile(data, encfnm)
        elif args.decrypt:
            encfnm, keyfnm, plainfnm = args.decrypt
            jf = JSONCrypt(keyfnm)
            data = jf.jsonfile_decrypt(encfnm)
            with open(plainfnm, "w") as outf:
                json.dump(data, outf)
        else:
            pass    # for completeness only
        sys.exit(0)
    except Exception as err:
        print("ERROR: " + str(err), file=sys.stderr)
        sys.exit(1)
    
if __name__ == "__main__":
    main()
