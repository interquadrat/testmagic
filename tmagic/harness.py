"""
Exercise harness infrastructure.
Provides the abstract base class 'Harness'.
:author: András Aszódi
:date: 2020-11-04
"""

from sys import stderr
from abc import ABC, abstractmethod

from .checker import DEFAULT_TOL, AlmostEqual

class Harness(ABC):
    """
    This abstract class can be configured to run code from an IPython cell
    and check its result.
    """

    def __init__(self, rel_tol=DEFAULT_TOL):
        """
        Creates a Harness instance.
        :param rel_tol: The relative tolerance to be used in comparing floats.
        """
        self._aeq = AlmostEqual(rel_tol)
        self._last = None

    def test_expr(self, expval, cell):
        """
        This method serves as the back-end of the custom magics in the ExerMagic class.
        It runs the contents of a cell
        and compares the result to an expected value which was registered before.
        :param expval: The expected result of the test. ExerMagic looks this up
            from its internal dictionary.
        :param cell: Possibly multi-line string, the contents of the cell
            decorated with `%%pyexer` or `%%rexer`.
        :return: True if the test passed, False otherwise (including exceptions)
        """
        try:
            # set up a "trial" context
            trialctx = self.get_trial_context()
            # execute the cell's contents in the trial context
            # always print the result to stdout
            obsval = self._exec_script(cell, ctx=trialctx, printres=True)
            # compare result to expected value
            ok = self._aeq.compare(expval, obsval)
            if ok:
                # re-run the cell in the global context
                # so that it's updated with the correct results...
                # a bit wasteful, but I couldn't find any other way
                # to implement the "commit/rollback" transaction semantics
                # for `eval`, `exec` and the `globals()`
                # this doesn't print the result because it was already there
                self._last = self._exec_script(cell)
            return ok
        except Exception as err:
            print(str(err), file=stderr)
            return False
    
    @property
    def last(self):
        return self._last
    
    @abstractmethod
    def get_trial_context(self):
        """
        Sets up a temporary "trial" execution context and returns it.
        When this is passed to `_exec_script()`, then the code executed
        is not influencing the global interpreter's state.
        Derived classes must implement this method according to the language requirements
        (whether Python, R or something else). The type of the return value
        depends also on the language.
        :return: An execution content appropriate for the language being run
        """
        raise NotImplementedError("Must implement 'get_trial_context()' method in a derived class")
        
    # -- "hidden" methods --

    @abstractmethod
    def _exec_script(self, script, ctx=None, printres=False):
        """
        Executes a script and returns the value of the last expression,
        or None if the last command was not an expression.
        Derived classes must implement this method according to the 
        language requirements (Python on R).
        :param script: Code as a possibly multi-line string.
        :param ctx: The execution context. If `None` (default), then
            the global interpreter is used.
        :param printres: The last result of a tested expression is printed to `stdout`
            (do not print by default)
        :return: The value of the last expression seen, or None
        """
        raise NotImplementedError("Must implement '_exec_script()' method in a derived class")

