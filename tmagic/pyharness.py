"""
Module providing the Python exercise harness class.
:author: András Aszódi
:date: 2022-08-25
"""

import sys
from copy import deepcopy
import ast

# Possible Tornado bug
### DELETE ME once fixed
"""
Exception ignored in: <function BaseEventLoop.__del__ at 0x7f708ad16ac0>
Traceback (most recent call last):
  File "/opt/conda/lib/python3.11/asyncio/base_events.py", line 691, in __del__
    if not self.is_closed():
           ^^^^^^^^^^^^^^^^
  File "/opt/conda/lib/python3.11/asyncio/base_events.py", line 688, in is_closed
    return self._closed
           ^^^^^^^^^^^^
AttributeError: '_UnixSelectorEventLoop' object has no attribute '_closed'
"""
# --- Monkey-patch the Tornado bug ---
# see https://stackoverflow.com/a/13977992, Andrew Clark's comment
import asyncio
_BEL = asyncio.base_events.BaseEventLoop
_original_del = _BEL.__del__
def _patched_del(self):
    try:
        _original_del(self)
    except:
        pass
_BEL.__del__ = _patched_del
# --- end of monkey patch ---

# Possible Tornado bug hack
import asyncio.base_events

from .harness import Harness
from .checker import DEFAULT_TOL
from .ipython import IPYTHON

class PyHarness(Harness):
    """
    This class can be configured to run Python code from an IPython cell
    and check its result.
    """
    
    # IPython user's global namespace context reference
    GLOBAL_CONTEXT = IPYTHON.instance.user_ns  ### or .ns_table["user_global"]
    
    def __init__(self, rel_tol=DEFAULT_TOL):
        """
        Creates a PyHarness instance.
        :param rel_tol: The relative tolerance to be used in comparing floats.
        """
        super().__init__(rel_tol=rel_tol)

    def get_trial_context(self):
        """
        Sets up a temporary "trial" execution context and returns it.
        When this is passed to `_exec_script()`, then the code executed
        is not influencing the global interpreter's state.
        :return: A dictionary containing a deep copy of all objects
            that have been created by the user.
        """
        # Deep copy of namespace dict entries inspired by https://stackoverflow.com/a/75957163
        ctx = {}
        for k,v in self.GLOBAL_CONTEXT.items():
            if k == "ctx":
                # skip the target dict itself... who knows
                continue
            try:
                if isinstance(v, asyncio.base_events.BaseEventLoop):
                    # hack to paper over possible `tornado` bug
                    # problem described in https://stackoverflow.com/q/75760275
                    raise TypeError("don't deep copy async event loop")
                tmpd = deepcopy({k:v})
                ctx.update(tmpd)
            except TypeError:
                # raised e.g. for modules that cannot be deep-copied
                # and for `_UnixSelectorEventLoop` objects
                ctx.update({k:v})
        return ctx
    
    # -- "hidden" --

    def _exec_script(self, script, ctx=None, printres=False):
        """
        Executes a script and returns the value of the last expression,
        or None if the last command was not an expression.
        Derived classes must implement this method according to the 
        language requirements (straightforward in Python, less so in R).
        :param script: Code as a possibly multi-line string.
        :param ctx: The execution context. If `None` (default), then
            the default IPython context is used.
        :param printres: The last result of a tested expression is printed to `stdout`
            (do not print by default)
        :return: The value of the last expression seen, or None
        """
        if ctx is None:
            ctx = self.GLOBAL_CONTEXT
        # Source: https://stackoverflow.com/a/76636602
        lastval = None
        lastexpr = None
        parsed = ast.parse(script)
        if parsed.body:
            lastcmd = parsed.body[-1]
            if isinstance(lastcmd, ast.Expr):
                lastexpr = ast.unparse(parsed.body.pop())
            elif isinstance(lastcmd, ast.Assign):
                lastexpr = ast.unparse(lastcmd.targets[0])
            elif isinstance(lastcmd, (ast.AnnAssign, ast.AugAssign)):
                lastexpr = ast.unparse(lastcmd.target)
        exec(ast.unparse(parsed), ctx)
        if lastexpr:
            lastval = eval(lastexpr, ctx)
        if printres and lastval is not None:
            print(lastval)
        return lastval
