"""
This module provides a class that encapsulates the IPython interactive shell instance
that is needed by some of the TestMagic classes.
:author: András Aszódi
:date: 2023-02-13
"""

import warnings # to suppress RPy2's complaints about Pandas missing

from IPython.core.getipython import get_ipython

class _IPy:
    """
    Small class that encapsulates the IPython interactive shell instance.
    We keep a single instance of it in this module ("sort-of-singleton" pattern)
    """
    
    def __init__(self):
        """
        Finds the current IPython instance and stores a reference to it.
        If the RPy2 package is available then it also loads the necessary extension.
        """
        self._ipy = get_ipython()
        self._has_rpy2 = self._load_rpy2_ext()
    
    @property
    def instance(self):
        """
        :returns: the current IPython instance if available
        :raises: RuntimeError if there is no IPython
        """
        if self._ipy is None:
            raise RuntimeError("IPython not available")
        return self._ipy

    @property
    def is_avail(self):
        """
        :returns: True if IPython is available, False otherwise.
        """
        return self._ipy is not None
    
    @property
    def rpy2_avail(self):
        """
        :returns: True if the RPy2 extensions have been loaded, False otherwise.
        """
        return self._has_rpy2

    # -- "private" methods --
    
    def _load_rpy2_ext(self):
        """
        Attempts to load the RPy2 extensions.
        :returns: True on success, False on failure.
        """
        # When RPy2 is imported, it complains that Pandas is not there.
        # We don't need no Pandas
        if not self.is_avail:
            return False
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            try:
                self.instance.run_line_magic("load_ext", "rpy2.ipython")
                return True
            except:
                return False
    
    
# Clients will refer to this module-level instance (modules ARE singletons in Python)
IPYTHON = _IPy()
