"""
The `quiz` module provides classes that support multiple-choice tests
based on J.M.Shea's `jupyterquiz` package.
:date: 2023-02-03
:author: András Aszódi
"""

import json
import unittest

from jupyterquiz import display_quiz

from .secret import JSONCrypt

# -- Classes --

# Note: The Question and Answer classes are supposed to reduce
# the tedium associated with writing JSON files required by `jupyterquiz`.

class Answer:
    """
    An `Answer` instance represents an answer to a multiple-choice quiz question.
    """
    
    def __init__(self, answer, correct=False, feedback=None):
        """
        Creates an Answer instance.
        :param answer: If a dictionary, then the `from_dict` method is used
            to initialise the instance. If a string, then this is the answer text.
        :param correct: `True` (default `False`)
        :param feedback: Explanatory text printed if the answer is chosen.
            If `None` (the default) then it will be set to "Correct" if `correct=True`
            and "False, please try again" if `correct=False`. You can override
            the default behaviour of course but make sure the feedback text makes sense :-)
        """
        self._answer = None
        self._correct = None
        self._feedback = None

        if isinstance(answer, dict):
            diag = self.from_dict(answer)
            if diag != "OK":
                raise ValueError(diag)
        elif isinstance(answer, str):
            self._answer = answer
            self.set_correct(correct, feedback)
        else:
            raise ValueError("Answer(answer) must be dict or str")

    @property
    def answer(self):
        return self._answer

    @property
    def correct(self):
        return self._correct

    @property
    def feedback(self):
        return self._feedback

    def set_correct(self, correct, feedback=None):
        """
        Sets the `correct` attribute of the calling object.
        :param correct: Boolean value indicating whether the Answer is correct or incorrect.
        :param feedback: If `None` (the default), then the `feedback` attribute of the calling object
            will be set to "Correct" or "False, please try again", according to whether `correct` was `True` or `False`.
            Otherwise sets the `feedback` attribute. Make sure you put something sensible here ;-)
        """
        self._correct = correct
        if feedback is None:
            self._feedback = "Correct" if correct else "False, please try again"
        else:
            self._feedback = feedback

    def set_feedback(self, feedback):
        """
        Sets the `feedback` attribute.
        :param feedback: The new value of the `feedback` attribute
        """
        self._feedback = feedback

    def from_dict(self, d):
        """
        Overwrites the calling object with the contents of a dictionary which
        must contain the keys "answer", "correct" and "feedback", 
        with str, bool, str values, respectively.
        If these conditions are not met, the calling object is not modified.
        :param d: A dictionary
        :returns: The string "OK" if `d` could be parsed. Otherwise a human-readable
            error message.
        """
        # local function to get a value
        def get_value(varname, typ):
            var = d[varname]
            if not isinstance(var, typ):
                raise ValueError(f"{var} is not {typ}")
            return var

        try:
            ans = get_value("answer", str)
            cor = get_value("correct", bool)
            fee = get_value("feedback", str)
            self._answer = ans
            self._correct = cor
            self._feedback = fee
            return "OK"
        except (KeyError, ValueError) as err:
            return "ERROR: " + str(err)
                
    @property
    def to_dict(self):
        """
        :returns: The calling object as a Python dictionary, ready to be written to a JSON file.
        """
        return { "answer": self.answer, "correct": self.correct, "feedback": self.feedback }

class Question:
    """
    A `Question` instance represents a multiple-choice quiz question and its answers.
    It can be converted from/to a dictionary.
    """
    
    def __init__(self, question, answers=None):
        """
        Creates a Question instance.
        :param question: A Python dictionary (possibly loaded from JSON) or a string
            which is then interpreted as the question text.
        :param answers: By default `None`, in this case you can add `Answer` objects later.
            Or an Answer object or a list of answers. Ignored if `question` was a dictionary.
        """
        self._question = None
        self._answers = None

        if isinstance(question, dict):
            diag = self.from_dict(question)
            if diag != "OK":
                raise ValueError(diag)
        elif isinstance(question, str):
            self._question = question
            if answers is None:
                self._answers = []
            elif isinstance(answers, Answer):
                self._answers = [answers]
            elif all(isinstance(a, Answer) for a in answers):
                self._answers = answers
            else:
                raise ValueError("Question(...,answers): answers must be None, Answer or list of Answers")
        else:
            raise ValueError("Question(question) must be dict or str")
    
    def from_dict(self, d):
        """
        Overwrites the calling object with settings from a dictionary
        if it contains the correct keys. Otherwise no changes are made.
        :param d: A Python dictionary with the keys "question" and "answers".
            The values are str and a list of dictionaries which correspond to Answer objects.
        :returns: "OK" if all went well or a human-readable error string on errors.
        """
        try:
            q = d["question"]
            if not isinstance(q, str):
                raise ValueError("question must be a string")
            ans = d["answers"]
            if not isinstance(ans, list):
                raise ValueError("answers must be a list")
            answers = [ Answer(a) for a in ans ]
            self._question = q
            self._answers = answers
            return "OK"
        except (KeyError, ValueError) as err:
            return "ERROR: " + str(err)
        
    def add_answer(self, answer):
        """
        Adds an `Answer` instance to the calling object.
        :param answer: Must be an `Answer` object.
        """
        if isinstance(answer, Answer):
            self._answers.append(answer)
        else:
            raise ValueError("Question.add_answer: expects Answer instance")
    
    @property
    def has_correct_answer(self):
        """
        :returns: True if there is at least one "correct" answer specified.
            This is a sanity check, to be run before the JSON is written.
        """
        return self._answers is not None and any(a.correct for a in self._answers)
            
    @property
    def to_dict(self):
        """
        :returns: The calling object as a Python dictionary, ready to be written to a JSON file,
            or None if the calling object has no correct answers (sanity check)
        """
        if not self.has_correct_answer:
            return None
        
        # NOTE: The object layout corresponds to the example at
        # https://github.com/jmshea/jupyterquiz
        # This class does not support the "numeric" type quiz questions
        q = { "question": self._question,
            "type": "many_choice" }
        answers = [a.to_dict for a in self._answers]
        q["answers"] = answers
        return q

class Quiz:
    """
    A `Quiz` instance represents a dictionary of `Question` objects.
    The keys are string identifiers of a particular question, the values describe a quiz question itself.
    This way all quiz questions for a notebook can be kept together and loaded once. The individual questions
    can be spread around the notebook and can be passed to the `jupyterquiz.display_quiz()` method.
    """
    def __init__(self, questions, keyfile=None):
        """
        Sets up a `Quiz` instance from a dict of Question objects, dict of dictionaries, or a JSON file containing a list of dicts.
        :param questions: either a dict of Question objects, a dict of dicts that can be converted to Question objects,
            or a JSON file containing a dict of dicts. Each dict value will be interpreted as a Question object.
        :param keyfile: If not the default (`None`), then this string is supposed
            to be an encryption key file with which the contents of
            an obfuscated JSON file (passed as `questions` can be decrypted. Otherwise it is ignored.
        """
        self._questions = {}
        if isinstance(questions, dict):
            diag = self.from_dict(questions)
        elif isinstance(questions, str):
            diag = self.from_file(questions, keyfile)
        else:
            diag = "questions must be dict or file name"
        if diag != "OK":
            raise ValueError(diag)

    def from_dict(self, qd):
        """
        Try to interpred `qd` either as a dict of string:dict or string:Question items,
        and if successful, then save it in the `_questions` member of the calling object.
        :param qd: dictionary of string:Question items or string:dict items where the values
            can be converted to `Question` objects.
        :returns: "OK" if the conversion was successful or a human-readable error message otherwise.
            In the latter case the calling object is not modified.
        """
        # local function
        def value_to_question(val):
            if isinstance(val, dict):
                return Question(val)
            elif isinstance(val, Question):
                return val
            else:
                raise ValueError("value must be dict or Question")

        try:
            self._questions = { key: value_to_question(val) for key, val in qd.items() }
            return "OK"
        except ValueError as err:
            return str(err)

    def from_file(self, jsonfnm, keyfilenm=None):
        """
        Reads a dict of str:Question items from an optionally encrypted JSON file
        and saves the result in the `_questions` member of the calling object if the operation was successful.
        :param jsonfnm: The name of a JSON file
        :param keyfilenm: if not None, an encryption key file with which `jsonfnm` should be decrypted first.
        :returns: "OK" if all went well, or a human-readable error message. In the latter case
            the calling object is not modified.
        """
        try:
            if keyfilenm is not None:
                # `jsonfnm` is assumed to be encrypted, let's decrypt
                jc = JSONCrypt(keyfilenm)
                qd = jc.jsonfile_decrypt(jsonfnm)
            else:
                with open(jsonfnm) as jsonf:
                    qd = json.load(jsonf)
            return self.from_dict(qd)
        except Exception as err:
            return str(err)

    @property
    def questions(self):
        return self._questions

    @property
    def to_dict(self):
        """
        Converts the calling object to a dictionary of dictionaries.
        Can be used to write to a JSON file.
        :returns: A dictionary of str: dict items
        """
        return { k: q.to_dict for k, q in self._questions.items() }

    def display(self, qkey):
        """
        Wrapper method around `jupyterquiz.display_quiz()`. Retrieves
        the Question object stored under the key `qkey`, converts it into
        a JSON-style string (a list containing a dict)
        and invokes `jupyterquiz.display_quiz()`.
        :param qkey: A string that identifies a particular `Question` object stored in the calling object.
        """
        q = self._questions[qkey]
        ql = [ q.to_dict ]
        display_quiz(ql)    # this works only in a Jupyter notebook: no unit test

